
mutable struct GitRepo
    url::String
    fallback_url::String
    dest::String
    branch::String
    tag::String
    prefer_fallback::Bool

    function GitRepo(url::AbstractString, dest::AbstractString; fallback_url::AbstractString="",
                     branch::AbstractString="master", tag::AbstractString="", prefer_fallback::Bool=false)
        new(url, fallback_url, dest, branch, tag, prefer_fallback)
    end
end

Base.rm(gr::GitRepo; recursive=true, kw...) = rm(gr.dest; recursive, kw...)
Base.dirname(gr::GitRepo) = gr.dest
Base.cd(𝒻, gr::GitRepo) = cd(𝒻, dirname(gr))
Base.cd(gr::GitRepo) = cd(dirname(gr))
Base.isdir(gr::GitRepo) = isdir(dirname(gr))

"""
    attempt(𝒻, gr::GitRepo)

Run the function `𝒻` taking the URL of the repo `gr` as an argument.  If execution fails with a
`ProcessFailedException`, `𝒻` will be run again but with the fallback URL provided instead.
Such a failure will also cause the `GitRepo` to prefer the fallback.

Calling this function on a `GitRepo` with `prefer_fallback` will use the fallback and will not
re-attempt a failure.
"""
function attempt(𝒻, gr::GitRepo)
    # if we already need to use the fallback, don't do anything weird, just use it
    gr.prefer_fallback && return 𝒻(gr.fallback_url)
    try
        𝒻(gr.url)
    catch e
        e isa ProcessFailedException || rethrow(e)
        isempty(gr.fallback_url) && rethrow(e)
        @warn("could not use git repo url $(gr.url); using fallback $(gr.fallback_url)")
        gr.prefer_fallback = true
        𝒻(gr.fallback_url)
    end
end

"""
    clone(gr::GitRepo; replace=false)

Clone the `gitrepo`.  If `replace`, will delete the existing directory, otherwise, this will fail if
the `GitRepo` destination exists.
"""
function clone(gr::GitRepo; replace::Bool=false)
    if ispath(gr.dest)
        if replace
            rm(gr, recursive=true, force=true)
        else
            error("can't clone $gr; destination exists, use `replace=true` to replace")
        end
    end
    attempt(url -> run(`git clone $url $(dirname(gr))`), gr)
end

"""
    checkout(gr::GitRepo)

Checkout the specified branch of the `GitRepo`.  If the repo does not exist locally `clone(gr)` will be run first.
"""
function checkout(gr::GitRepo)
    isdir(gr) || clone(gr)
    arg = isempty(gr.tag) ? gr.branch : gr.tag
    cd(() -> run(`git checkout $arg`), gr)
end

"""
    pull(gr::GitRepo)

Pull the specified branch from `origin`.  If the repo does not exist locally `clone(gr)` will be run first.
"""
function pull(gr::GitRepo)
    isdir(gr) || clone(gr)
    cd(() -> run(`git pull origin $(gr.branch)`), gr)
end
