module Installer

const DOTFILES_DIR = Ref{String}()
const HOME_DIR = Ref{String}()

import REPL, Downloads
using REPL.TerminalMenus


include("git.jl")


function _configdir!(v::Ref, env::AbstractString, default::AbstractString; desc::AbstractString="")
    v[] = if env ∈ keys(ENV)
        ENV[env]
    else
        isempty(desc) || @warn("$desc not defined. it can be set with the environment variable `$env`.  "*
                               "defaulting to $default")
        default
    end
end

"""
    dotfilesdir!([dir])

Set the directory from which the configuration dotfiles will be installed.  By default, this will be taken from
the environment variable `INSTALLER_DOTFILES_DIR`.
"""
dotfilesdir!() = _configdir!(DOTFILES_DIR, "INSTALLER_DOTFILES_DIR", joinpath(pwd(), "etc"); desc="dotfiles directory")
dotfilesdir!(dir::AbstractString) = (DOTFILES_DIR[] = dir)
dotfilesdir() = DOTFILES_DIR[]

homedir!() = _configdir!(HOME_DIR, "HOME", "/root"; desc="home directory")
homedir() = HOME_DIR[]

"""
    issudo()

Determines whether the current user has `sudo` priveledges.
"""
issudo() = parse(Int, readchomp(`id -u`)) == 0

"""
    hasfuse()

Determines whether the [FUSE](https://wiki.archlinux.org/index.php/FUSE) device is available.
If `false`, AppImages cannot be run without extracting.
"""
hasfuse() = ispath("/dev/fuse")

"""
    getdistroinfo()

Get a string which describes the current linux distribution.  This is from scanning through `/etc/` for files
with `-release` in the name.
"""
function getdistroinfo()
    join((String(open(read, f)) for f ∈ readdir("/etc", join=true) if occursin("-release", f)), '\n')
end

"""
    distro()

Return a `Symbol` describing the current linux distribution.  This is primarily used for selecting a package manager,
so Manjaro and Arch are equivalent, as are debian and ubuntu.
"""
function distro()
    str = lowercase(getdistroinfo())
    if any(occursin(s, str) for s ∈ ("manjaro", "arch linux"))
        :arch
    elseif any(occursin(s, str) for s ∈ ("debian", "ubuntu"))
        :debian
    else
        @warn("could not determine linux distro", release=str)
    end
end

const DISTRO = Val{distro()}

"""
    srcdstpaths(fname)

Generate source and destination file names for dotfiles.
"""
srcdstpaths(fname) = (joinpath(dotfilesdir(),fname), joinpath(homedir(),fname))

function _shouldwrite(dst; confirm::Bool=isinteractive())
    if isfile(dst) && confirm
        request("$dst exists, replace?", RadioMenu(["yes","no"], charset=:unicode)) == 1
    else
        true
    end
end

"""
    diff(src, dst; confirm=isinteractive())

If `confirm`, prompt the user whether they'd like to edit the diff between the files `src` and `dst`.
"""
function diff(src, dst; confirm::Bool=isinteractive())
    if !confirm
        isfile(dst) && @warn("will replace existing file $dst")
        return true
    end
    if isfile(dst)
        r = request("$dst exists, replace?", RadioMenu(["yes", "no", "edit diff"], charset=:unicode))
        r == 3 && run(`$(ENV["EDITOR"]) -d $src $dst`)
        return r == 1
    end
    true
end

function _pushfile(src::AbstractString, dst::AbstractString; confirm::Bool=isinteractive())
    if diff(src, dst; confirm)
        mkpath(dirname(dst))
        cp(src, dst, force=true, follow_symlinks=true)
        @info("wrote $src to $dst")
    end
end
function _pushdir(path::AbstractString; confirm::Bool=isinteractive())
    for c ∈ readdir(joinpath(dotfilesdir(), path))
        s, d = srcdstpaths(joinpath(path, c))
        _pushfile(s, d; confirm)
    end
end

"""
    push(file; confirm=isinteractive())

Copy a file from the dotfiles directory to the home directory.  If `confirm` and a file already exists,
the user will be given the option to whether to replace or edit a diff.  The path of `file` should be given
relative to the home directory.
"""
function push(file::AbstractString; confirm::Bool=isinteractive())
    src, dst = srcdstpaths(file)
    isdir(src) ? _pushdir(file; confirm) : _pushfile(src, dst; confirm)
end

"""
    pull(file; confirm=isinteractive())

Copy a file from the home directory to the dotfiles directroy.  This is useful for updating the dotfiles repo
after changes have been made to the file on the system.  If `confirm` and a file already exists,
the user will be given the option whether to replace or edit a diff.  The path of `file` should be given
relative to the home directory.
"""
function pull(file::AbstractString; confirm::Bool=isinteractive())
    dst, src = srcdstpaths(file)
    isdir(src) ? _pushdir(file; confirm) : _pushfile(src, dst; confirm)
end

"""
    installconfig(cfg)

Install the configuration file `cfg`.  This will copy it from the `dotfilesdir()` source dir to the same relative path
in `HOME`.
"""
installconfig(cfg::AbstractString; confirm::Bool=isinteractive()) = push(cfg; confirm)

"""
    installconfigs

Install all configs in a file, or all configs specified by a `Program` object.
"""
function installconfigs end


include("Programs.jl")
using .Programs: Program, Download, name

"""
    programs()

Get a list of all configured programs.  This is obtained from a list of `Program` objects in the `Programs` submodule.
"""
function programs()
    ps = Vector{Program}(undef, 0)
    for n ∈ names(Programs, all=true)
        p = getproperty(Programs, n)
        p isa Program && push!(ps, p)
    end
    ps
end

installconfigs(p::Program; confirm::Bool=isinteractive()) = foreach(c -> installconfig(c; confirm), p.configs)

"""
    pullrepos(p)

Pull all repositories associated with the program config `p`, cloning if necessary.  This will be done with the
`git pull` command so any discrepancies need to be resolved manually later.
"""
pullrepos(p::Program) = foreach(pull, p.repos)

"""
    download(d::Download)

Download the file specified by the `Download` object `d`.
"""
function Downloads.download(d::Download)
    dst = joinpath(homedir(), d.dst)
    mkpath(dirname(dst))
    # weird shit can happen to curl if it tries to overwrite
    isfile(dst) && rm(dst)
    Downloads.download(d.url, dst)
    d.exe && run(`chmod a+x $dst`)
    @info("downloaded $(d.url) to $dst")
end

"""
    downloads(p)

Perform all downloads for a `Program` object.
"""
function downloads(p::Program; confirm::Bool=isinteractive())
    for d ∈ p.downloads
        dst = joinpath(homedir(),d.dst)
        _shouldwrite(dst; confirm) && Downloads.download(d)
    end
end

"""
    runcommand(cmd)

If in interactive mode, prompt whether or not a command should be run.
"""
function runcommand(cmd::Cmd; confirm::Bool=isinteractive())
    if confirm
        request("run command $cmd ?", RadioMenu(["yes", "no"], charset=:unicode)) == 1
    else
        true
    end && run(cmd)
end

runcommands(p::Program; confirm::Bool=isinteractive()) = foreach(c -> runcommand(c; confirm), p.commands)

"""
    pminstall_command

Return the command for installing a program, depending on linux distro.
"""
pminstall_command(::Type{Val{:arch}}, args...) = Cmd(String["pacman", "--noconfirm", "-S", args...])
pminstall_command(::Type{Val{:debian}}, args...) = Cmd(String["apt-get", "install", "-y", args...])

"""
    pmupdate_command

Return the command for performing a package manager update.
"""
pmupdate_command(::Type{Val{:arch}}) = Cmd(["pacman", "-Syyu"])
pmupdate_command(::Type{Val{:debian}}) = Cmd(["apt-get", "update"])

"""
    pminstall(args...)
    pminstall(p::Program)

Perform a package manager installation.  Arguments will be provided to the standard package manager install.

Will prompt before running if in interactive mode.  Will skip if not sudo.
"""
function pminstall(args...; confirm::Bool=isinteractive())
    cmd = pminstall_command(DISTRO, args...)
    if !issudo()
        @warn("must be sudo to run: $(string(cmd)); will skip and attempt to proceed")
        return
    end
    confirm && (request("run command $cmd ?", RadioMenu(["yes", "no"])) == 1 || return)
    @info("running command: $(string(cmd))")
    run(cmd)
end

pminstall(p::Program; confirm::Bool=isinteractive()) = foreach(c -> pminstall(c; confirm), p.packages)

"""
    install(p; confirm=isinteractive())

Install the program `p`.  If `confirm`, the user will be asked for confirmation for potentially destructive actions
such as overwriting files.
"""
function install(p::Program; confirm::Bool=isinteractive(), kw...)
    @info("... installing $(name(p)) ...")
    downloads(p; confirm)
    installconfigs(p; confirm)
    pullrepos(p)
    runcommands(p; confirm)
    Programs.aux(p; confirm, kw...)
    pminstall(p; confirm)
    @info("___ done with $(name(p)) ___")
end

"""
    install(progs)
    install(p)

Install a program or list of programs, performing all steps specified by that program's
configuration.  In interactive mode, installing a list of programs will prompt with a selection menu
from that list.
"""
function install(progs=programs())
    idx = if isinteractive()
        collect(request("Select programs to install:", MultiSelectMenu(string.(name.(progs)))))
    else
        eachindex(progs)
    end
    foreach(install, progs[idx])
end

"""
    addprogramfile(fname)

Include a Julia source file containing program install configurations in the `Programs` submodule.
"""
addprogramfile(fname::AbstractString) = Base.include(Programs, fname)

"""
    addprograms(dir)

Include all Julia source files in a directory to the program install configurations in the `Programs` submodule.
"""
function addprograms(dir::AbstractString)
    for f ∈ filter(s -> endswith(s, ".jl"), readdir(dir, join=true))
        addprogramfile(f)
    end
end

"""
    help()

Display a useful help message.  This is displayed when the package is started in interactive mode.
"""
function help()
    printstyled("Installer.jl\n", bold=true, color=:blue)
    isinteractive() && printstyled("""
        - `help()`: this message
        - `install()`: select from a menu of programs
        - `install(p::Program)`: install specific program
        - `addprograms(dir)`: Add program install configs from Julia source code for all files in directory `dir`.
        - `Programs`: module containing programs
        - `programs()`: list of programs in `Programs`
        - `push(file)`: copies a file from the dotfiles to the system
        - `pull(file)`: copies a file from the system to the dotfiles (e.g. to update repo)\n
    """, color=:light_magenta)
    issudo() || printstyled("you are not currently sudo, some installs may not work\n", color=:cyan)
end

function __init__()
    dotfilesdir!()
    homedir!()
    haskey(ENV, "INSTALLER_PROGRAMS_DIR") && addprograms(ENV["INSTALLER_PROGRAMS_DIR"])
    isinteractive() && help()
end


export issudo, help, install, installconfig, installconfigs, programs, addprogramfile, addprograms,
       push, pull
export Programs, Program, GitRepo

end # module
