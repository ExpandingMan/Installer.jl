module Programs

import Installer; const I = Installer
import Downloads # some may need this in aux

using Installer: GitRepo

# TODO: should deal with tarballs and such
"""
    Download

A struct describing a download needed by the installer.  Includes a source, destination, and whether or not the downloaded
file should be made executable.
"""
struct Download
    url::String
    dst::String
    exe::Bool
end
Download(url::AbstractString, dst::AbstractString) = Download(url, dst, false)

"""
    Program{name}

A struct describing the install configuration of a program.  Running
`install(p::Program)` performs all configured installation steps.
"""
struct Program{name}
    configs::Vector{String}
    downloads::Vector{Download}
    commands::Vector{Cmd}
    repos::Vector{GitRepo}
    packages::Vector{String}
    deps::Vector{Program}
    aux::Dict{Symbol,Any}
end


function Program(n::Symbol; kwargs...)
    Program{n}(get(kwargs, :configs, Vector{String}()), get(kwargs, :downloads, Vector{Download}()),
               get(kwargs, :commands, Vector{Cmd}()), get(kwargs, :repos, Vector{GitRepo}()),
               get(kwargs, :packages, Vector{String}()), get(kwargs, :deps, Vector{Program}()),
               get(kwargs, :aux, Dict{Symbol,Any}()),
              )
end

"""
    name(p::Program)

Return the name of a Program.
"""
name(p::Program{n}) where {n} = n

function Base.show(io::IO, p::Program)
    show(io, typeof(p))
    write(io, "(:",name(p),")")
    nothing
end

function Base.show(io::IO, ::MIME"text/plain", p::Program)
    print(io, "Program")
    print(io, "("); printstyled(io, name(p), color=:yellow, bold=true); print(io, ",\n")
    print(io, "\tconfigs="); printstyled(IOContext(io, :compact=>true), p.configs, color=:blue); print(io, ",\n")
    print(io, "\tdownloads="); printstyled(IOContext(io, :compact=>true), p.downloads, color=:blue); print(io, ",\n")
    print(io, "\tcommands="); printstyled(IOContext(io, :compact=>true), p.commands, color=:blue); print(io, ",\n")
    print(io, "\tpackages="); printstyled(IOContext(io, :compact=>true), p.packages, color=:blue); print(io, ",\n")
    print(io, ")")
    nothing
end

"""
    aux(p::Program)

Perform auxiliary installation procedure.  This should be defined for any program which requires installation steps which are
not easily expressed in the default installation steps.
"""
aux(::Program; kw...) = nothing


end # module Programs
