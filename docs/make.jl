using Installer
using Documenter

makedocs(;
    modules=[Installer],
    authors="Expanding Man <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Installer.jl/blob/{commit}{path}#{line}",
    sitename="Installer.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Installer.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "API" => "api.md"
    ],
)

deploydocs(;
    repo="gitlab.com/ExpandingMan/Installer.jl",
)
