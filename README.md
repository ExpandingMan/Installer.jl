# Installer

[![docs](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Installer.jl/)
[![build status](https://img.shields.io/gitlab/pipeline/ExpandingMan/Installer.jl/master?style=for-the-badge)](https://gitlab.com/ExpandingMan/Installer.jl/-/pipelines)

An installer for linux for installing programs and configurations that requires some
customization beyond what is available through package manager, designed especially for
the installation of user "dotfiles".

Programs and configs in my [dotfiles repo](https://gitlab.com/ExpandingMan/dotfiles) are
installed with this package.
